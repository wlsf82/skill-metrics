# WIP - Skill metrics

A plan for self-improvement.

## Objective

The idea of this project is to define the different skills necessary for a test automation engineer, and the small steps that need to be taken until acchieving solid experience in every specific domain.

Similar to the [GitLab's roadmap for the engineering quality department](https://about.gitlab.com/handbook/engineering/quality/roadmap/#overview), this plan will be separated into two sections, [track](#track) and [timeline](#timeline).

## Track

The tracks will be the skills, in a high-level, that need to be improved until the practitioner acchieves solid experience.

For every track there will be checkboxes that need to completed until reaching the solid experience in that specific track.

In some cases the knowledge that one is looking for is the knowledge that someone else in the team already have. In this case, we could think about a mentoring program so that more experienced practitioners in a specific domain can share their knowledge with the apprentices.

### Unit testing

It's important that a test automation engineer have a solid experience in unit testing so that he/she can evaluate during the software development cycle if the unit testing coverage of a specific feature is good enough, for instance, and also to evaluate the need of other kind of tests, such as integration and/or end-to-end tests. Such knowledge can also be valuable for reviewing unit testing code.

To gain such experience, practice is needed, and unit testing can be practiced in different ways, and at least the following should be required:

- [x] Knowledge of the available libraries for unit testing in a specific programming language.
  - [x] Ruby (MiniTest)
  - [x] JavaScript (Tape, Jasmine, Mocha, Chai, Jest)
- [x] Watching videos.
  - [x] https://youtu.be/Eu35xM76kKY
  - [ ] ...
- [x] Understanding and practice TDD (https://github.com/wlsf82/cv#test-driven-development---codecademy).
- [x] Evaluation of code coverage reports.
- [x] Practice of unit testing with some katas (e.g., [The TDD Course for Beginners: Learn by Practicing 7 katas - Udemy](https://www.udemy.com/draft/281018/)).
- [ ] Practice of unit testing using test doubles (mocks, stubs, spies, etc.)
- [x] A proof-of-concept of unit testing in a real project. ([Vidbits](https://github.com/wlsf82/vidbits-codecademy) and [Gather](https://github.com/wlsf82/gather-codecademy) - codecademy)
- [ ] Mentoring with a specialist (optional). 

### API testing

Knowing API testing is beneficial to a test automation engineer, not only because it allows for testing applications in the integration level, but also due to the fact that knowing how every part of the applicaiton communicates with each other helps on understanding how the the application works as a whole.

To gain such experience, practice is also needed, and integration testing can be practiced in different ways, and at least the following should be required:

- [ ] Knowledge of the available libraries for API testing in a specific programming language.
  - [x] JavaScript
  - [ ] Ruby
- [ ] Watching videos
  - [x] https://youtu.be/Hw522TDxMZk
  - [ ] https://www.youtube.com/watch?v=IOCcqIKJyFk
 - [ ] ...
- [x] Evaluation of code coverage reports.
- [x] Practice of API testing with hands-on exercises. (https://www.codecademy.com/pro/intensive/test-driven-development)
- [ ] Practice of API testing using test doubled (mocks, stubs, spies, etc.)
- [x] A proof-of-concept of API testing in a real project. ([Vidbits](https://github.com/wlsf82/vidbits-codecademy) and [Gather](https://github.com/wlsf82/gather-codecademy) - codecademy)
- [ ] Mentoring with a specialist (optional). 

### Contract testing

Contract testing can be a valuable technique when working with micro-services, for example.
A change in a specific service can break other services that consumes what the first one provides.
Knowing how to prevent such issues is something that a test automation engineers should be aware of, but to gain such experience, practice is needed.

Below are some suggestions to practice contract testing:

- [ ] Read documentation of pact.io
- [ ] Watching videos
  - [x] https://youtu.be/r-cUe2TJ2AU
  - [ ] ...
- [ ] POC of pact testing
- [ ] Practice of pact testing in a real, but not risky project
- [ ] Mentoring with a specialist (optional). 

### GUI testing

- [x] Read docs of RSpec
- [x] Understanding and practice the Page Objects pattern
- [x] Understanding and practice the usage of waits and expected conditions
- [x] Understanding and practice independent GUI tests
- [x] Use APIs and other mechanisms to inject state in the application to run GUI tests
- [x] Running tests in headless mode
- [x] Running tests in parallel (Selenium Grid)
- [x] Integration with test reports
- [x] A proof-of-concept of GUI testing in a real project. (https://github.com/wlsf82/curso-tat-protractor/tree/master/test/e2e)
- [ ] Watching videos
  - [x] https://youtu.be/ykYvkKWXPi8
  - [ ] ...

### Performance/load testing

- [x] POC with [Gatling.io](https://gatling.io/docs/current/quickstart/)
- [x] POC with [GitLab LoadKit](https://gitlab.com/andrewn/gitlab-load-kit)
- [ ] POC with [Artillery.io](https://artillery.io/docs/getting-started/#run-a-quick-test)
- [ ] ...


### Security testing

- [ ] Online training on Cross-site scripting (XSS) (https://xss-game.appspot.com)
- [ ] Read Big List of Naughty Strings (https://github.com/minimaxir/big-list-of-naughty-strings)
- [ ] Studies of [OWASP Zed Attack Proxy Project](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project)
- [ ] Studies of [Burp Suite](https://portswigger.net/burp)
- [ ] [Penetration testing](https://www.youtube.com/channel/UC0ZTPkdxlAKf-V33tqXwi3Q)
  - [x] [Password Cracking With John The Ripper - RAR/ZIP & Linux Passwords](https://www.youtube.com/watch?v=XjVYl1Ts6XI)
  - [ ] ...
- [ ] ...

### CI/CD

- [x] POC with Jenkins
- [x] POC with GoCD
- [x] POC with GitLab
- [X] POC with SemaphoreCI
- [x] Work with Jenkins in a real project
- [x] Work with GoCD in a real project
- [x] Work with GitLab in a real project
- [x] Work with SemaphoreCI in a real project
- [x] Write pipelines as code
  - [x] GoCD
  - [x] SemaphoreCI 2.0
  - [x] GitLab
- [ ] ...

### Cloud Computing

- [x] Taking a course of AWS for developers (http://www.solidit.com.br/servicos.treinamento.developer.html) 
- [x] Taking a course of AWS for business (http://www.solidit.com.br/servicos.treinamento.business.html)
- [x] POCs with EC2, S3, security groups
- [ ] Experiment with Google Cloud Platform
- [ ] Experiment with Microsoft Azure
- [ ] ...

### Containarization

- [x] [Course Scalling Tests with Docker - Test Automation University](https://testautomationu.applitools.com/scaling-tests-with-docker/)
- [X] [POC of Selenium Grid running on Docker to execute GUI tests in parallel](https://gitlab.com/wlsf82/scaling-tests-with-docker)
- [ ] ...

## Timeline

The timeline is the chronological order in which every item is expected to be completed.

TBD.